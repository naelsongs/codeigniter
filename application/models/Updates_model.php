<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Updates_model extends CI_Model
{
    public function getUsersFilter($identified)
    {
        $query = $this->db->select("*")->where('identified', $identified)->get('usuarios');
        return $query->result_array();
    }

    public function delete($identified)
    {

        $this->db->where('identified', $identified);

        try {
            $this->db->delete('usuarios');
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function update($name,$login,$email,$password,$dataCreated,$codAuthorization,$statusUser,$codePeople,$identified)
    {



        //Exception para garantir um retorno preciso
        try {

            $data = array(
                'nome' => $name,
                'login' => $login,
                'email' => $email,
                'senha' => md5($password),
                'data_criacao' => $dataCreated,
                'temp_expired_senha' => null,
                'cod_autorizacao' => $codAuthorization,
                'status_usuario' => $statusUser,
                'cod_pessoa' => $codePeople
                //'identified' => md5(rand(1, 99999)) //Generation new key for security flooding Modelo dev by Naelson
            );

            $this->db->set('nome','login','email','senha','data_criacao','temp_expired_senha','cod_autorizacao','status_usuario','cod_pessoa');
            $this->db->where('identified', $identified);
            $this->db->update('usuarios', $data);

            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }


    public function crudRead(){

        $query = $this->db->select("*")->get('usuarios');
        return $query->result_array();

    }

    public function crudUpdate($id,$newLogin){

        $this->db->set('login');
        $this->db->where('id', $id);
        try{
            $this->db->update('usuarios',array('login'=>$newLogin));
            return true;
        }catch (Exception $e){
            return false;
        }

    }

    public function crudDelete($id){

        $this->db->where('id', $id);
        try{
            $this->db->delete('usuarios');
            return true;
        }catch (Exception $e){
            return false;
        }

    }


}