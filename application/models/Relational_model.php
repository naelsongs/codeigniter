<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relational_model extends CI_Model
{

    public function filterProduct($name)
    {

        $this->db->select('nome,id_aparelho');
        $this->db->from('usuarios');
        $this->db->join('usuarios_aparelhos', 'usuarios.id = usuarios_aparelhos.id_usuario')->where('nome', $name);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }




    public function getProductById($id)
    {
        $query = $this->db->select("*")->where('id', $id)->get('aparelho');
        return $query->result_array();
    }



    public function filterProfile($name)
    {

        $this->db->select('nome,id_usuarios,id_perfil');
        $this->db->from('usuarios');
        $this->db->join('usuarios_perfil', 'usuarios.id_usuarios = usuarios_perfil.id_perfil')->where('nome', $name);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }


    public function getProfile($id)
    {
        $query = $this->db->select("*")->where('id', $id)->get('perfil');
        return $query->result_array();
    }

}