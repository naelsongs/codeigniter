<div class="col-md-12">

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Pesquisa por usuario aqui
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <form method="post" action="<?= site_url("profile/pdf")?>" target="_blank">


                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pesquisa se usuario tem profile</h5>
                        <button type="button" name="t_id_profile" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label for="name">Usuario</label>
                        <input type="text" name="t_name" class="form-control" id="name">
                        <input type="hidden" name="t_id_profile" id="id-profile" value="5">
                        <br>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" id="check">Checar</button>
                        <button type="submit" class="btn make btn-success" disabled='disabled'>Emitir</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <br>
    <hr/>
    <br>

    <ul class="list-group text-center">
        <?php for ($i = 0; $i < $countUsers; $i++) { ?>
            <li class="list-group-item">Users: <?= $nome[$i]; ?></li>
        <?php } ?>
    </ul>


</div>


<script type="application/javascript">
    $(function () {
        $('#check').click(function () {

            $.ajax({
                type: 'POST',
                url: "<?= base_url("ProductController/checkHaveProductUsersName"); ?>",
                data: {name: $('#name').val()},
                dataType: 'json',
                success: function (json) {


                    if (json.successful === true){
                        alert("Successful"+"\n"+"Name: "+json.name+"\n"+"Id Perfil: "+json.id);
                        $('input[name="t_id_profile"]').val(json.id);

                        $('.make').prop('disabled', false);
                    }

                    if (json.t === true)
                        alert("Nao Existte Produdos com este usuario");

                    if (json.empty === true)
                        alert("Field Vazio");
                },

                error:function () {console.log("ERROR EXCEPTION");}

            });

        });
    });
</script>