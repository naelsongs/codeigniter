<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Tecnica Criada e utilizada By Naelson -->
<style>
    #product-style {background-color: #28A745 !important; color: white; border-radius: 5px;}
</style>

<div class="col-md-12">

    <h1 class="display-4">Aparelho</h1>

<form onsubmit="return false" id="product">

    <div class="form-group">
        <label for="description">Descricao</label>
        <input type="text" name="t_description" class="form-control" id="description" aria-describedby="emailHelp">

    </div>

    <div class="form-group">
        <label for="code">Codigo</label>
        <input type="text" name="t_code" class="form-control" id="code" aria-describedby="emailHelp">
    </div>

    <button type="submit" class="btn btn-dark">Cadastrar</button>
</form>

</div>


<script type="application/javascript">

    $(function () {

        $('#product').submit(function (obj) {

            obj.preventDefault();

            console.log($(this).serialize());

            $.ajax({

                type: 'POST',
                url: "<?= base_url("InputController/createProduct"); ?>",
                data: $(this).serialize(),
                dataType: 'json',
                success: function (json) {

                    if (json.have === true)
                        alert("Ja Existe Codigo");

                    if (json.successful === true)
                        alert("Aparelho cadastrado com sucesso!");


                },

                error:function () {console.log("ERROR EXCEPTION");}
            });
        });
    });

</script>