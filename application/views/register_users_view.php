<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Tecnica Criada e utilizada By Naelson -->
<style>#users-style {background-color: #28A745 !important; color: white; border-radius: 5px;}</style>
<style type="text/css">
    .crud{
        list-style: none;
    }
</style>
<div class="col-md-12">
<h1 class="display-4 text-center">Cadastro</h1>
<h5 class=" text-center"><a role="button" href="<?= site_url("product")?>" class="btn btn-danger">Emitir Usuario PDF</a> <a  role="button" href="<?= site_url("profile")?>" class="btn btn-danger">Emitir Perfil PDF</a></h5>

<form class="w-25 m-auto" onsubmit="return false" id="create_account">

    <div class="form-group">
        <label for="name">Nome</label>
        <input type="text" name="target_name" class="form-control" id="name" aria-describedby="emailHelp" autocomplete required>
        <small id="emailHelp" class="form-text text-muted">Nome Completo</small>
    </div>

    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" name="target_login" class="form-control" id="login" aria-describedby="emailHelp" autocomplete required>
        <small id="emailHelp" class="form-text text-muted">Apelido</small>
    </div>

    <div class="form-group">
        <label for="target_email">Email</label>
        <input type="email" name="target_email" class="form-control" id="target_email" aria-describedby="emailHelp" autocomplete required>
    </div>

    <div class="form-group">
        <label for="target_password">Password</label>
        <input type="password" name="target_password" class="form-control" id="target_password" required>
    </div>

    <div class="form-group text-center">
        <button type="submit" class="btn-lg btn-dark">Cadastrar</button>
    </div>


</form>
    <br>

    <table class="table text-center">
        <thead>
        <tr>

            <th scope="col">NOME</th>
            <th scope="col">LOGIN</th>
            <th scope="col">EMAIL</th>
            <th scope="col">DATA CRIACAO</th>
            <th scope="col">COD AUTORIZACAO</th>
            <th scope="col">STATUS</th>
            <th scope="col">CODIGO PESSOA</th>
            <th scope="col">UPDATE</th>

        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0; $i < $countUsers; $i++) { ?>
            <tr>

                <td>
                    <?= $nome[$i]; ?>

                </td>
                <td><?= $login[$i]; ?></td>
                <td><?= $email[$i]; ?></td>
                <td><?= $data_criacao[$i]; ?></td>
                <td><?= $cod_autorizacao[$i]; ?></td>
                <td><?= $status_usuario[$i]; ?></td>
                <td><?= $cod_pessoa[$i]; ?></td>

                <td><a style="background-color: transparent; border: none;" id="<?php echo "id-edit".$i; ?>" href="<?= site_url("user/show?identified=".$identified[$i]); ?>"><i class="fas fa-wrench"></i></a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>


</div>



<script type="application/javascript">

    $(function () {

        $('#create_account').submit(function (obj) {

            obj.preventDefault();

            console.log($(this).serialize());

            $.ajax({

                type: 'POST',
                url: "<?= base_url("InputController/createAccount"); ?>",
                data: $(this).serialize(),
                dataType: 'json',
                success: function (json) {

                    if (json.successful === true){
                        alert("Cadastro efetuado com sucesso!");
                        location.reload();
                    }


                    if (json.email === true)
                        alert("Ja existe Email")

                    if (json.login === true)
                        alert("Ja existe Login")

                },

                error:function () {console.log("ERROR EXCEPTION");}

            });
        });
    });

</script>