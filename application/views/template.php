<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Autor Naelson Gonçalves Saraiva  Development with last version technology of merchant-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script type="application/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

      <title>Crud</title>

      <style>
          .btn-style {border-bottom: 3px solid transparent; font-size: 16px; padding: 10px;}
          .btn-style:hover {border-bottom: 3px solid black !important; color: black !important;}
          .top-style{height: 56px; width: 100%; background-color: #F8F9FA; position: absolute;border-bottom: 3px solid black !important;}
      </style>

  </head>
  <body>


   <div class="top-style"></div>

   <div class="container">

       <div class="row">

           <?php  $this->load->view('header_view'); ?>

       </div>

       <br>

       <div class="row">

            <?php  $this->load->view($content); ?>

       </div>

   </div>

<!--   <div style="position: absolute; bottom:  0; right: 10px;">-->
<!---->
<!--       <h6 class="text-secondary">Development By<strong><small> https://naelsonbr.github.io/</small></strong></h6>-->
<!---->
<!--   </div>-->

    <!-- Optional JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>