<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function index(){}

    public function registerUser()
    {

        $arrays = array(
            'content' =>'register_users_view',
            'countUsers' => $this->users_model->getUsersCount(),
            'id' => array(),
            'nome' => array(),
            'login' => array(),
            'email' => array(),
            'senha' => array(),
            'data_criacao' => array(),
            'temp_expired_senha' => array(),
            'cod_autorizacao' => array(),
            'status_usuario' => array(),
            'cod_pessoa' => array(),
            'identified' => array()

        );

        $i = 0;
        foreach ($this->users_model->getUsers() as $key){

            $arrays['id'][$i] = $key['id'];
            $arrays['nome'][$i] = $key['nome'];
            $arrays['login'][$i] = $key['login'];
            $arrays['email'][$i] = $key['email'];
            $arrays['senha'][$i] = $key['senha'];
            $arrays['data_criacao'][$i] = $key['data_criacao'];
            $arrays['temp_expired_senha'][$i] = $key['temp_expired_senha'];
            $arrays['cod_autorizacao'][$i] = $key['cod_autorizacao'];
            $arrays['status_usuario'][$i] = $key['status_usuario'];
            $arrays['cod_pessoa'][$i] = $key['cod_pessoa'];
            $arrays['identified'][$i] = $key['identified'];
            $i ++;
        }

        $this->load->view('template',$arrays);
    }


    public function registerProduct()
    {

        $arrays = array('content' =>'register_product_view');
        $this->load->view('template',$arrays);
    }

    public function registerProfile()
    {

        $arrays = array('content' =>'register_profile_view');
        $this->load->view('template',$arrays);
    }

}
