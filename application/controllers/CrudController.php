<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class CrudController extends CI_Controller
{

    public function list()
    {


         //print_r($this->updates_model->crudRead());

        if (isset($_GET)) {

            echo json_encode($this->updates_model->crudRead());

        }else {
            echo json_encode("Error retornado");
        }
    }


    public function update()
    {

        if (isset($_POST['id']) AND !empty($_POST['newLogin']) ) {

            echo json_encode(array('successful' =>  $this->updates_model->crudUpdate($_POST['id'],$_POST['newLogin'])));

        }else {
            echo json_encode("Error retornado");
        }
    }


    public function delete()
    {

        if (isset($_POST['id'])) {

            echo json_encode(array('successful' =>  $this->updates_model->crudDelete($_POST['id'])));

        }else {
            echo json_encode("Error retornado");
        }
    }




}