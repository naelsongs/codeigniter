<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Author N G S
 * Data 06/04/2019
 **/
class InputController extends CI_Controller
{

    public function index()
    {
    }

    public function createAccount()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            if ($this->users_model->verifyEmail($_POST['target_email']) == 1) {

                echo json_encode(array('email' => true));

            } else if ($this->users_model->verifyLogin($_POST['target_login']) == 1) {

                echo json_encode(array('login' => true));

            } else {

                if ($this->users_model->insertDataUser($_POST['target_name'], $_POST['target_login'], $_POST['target_email'], $_POST['target_password']) === true) {

                    echo json_encode(array('successful' => true));

                } else {

                    echo json_encode(array('successful' => false));

                }
            }
        }
    }

    public function createProduct()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {


            if ($this->users_model->verifyProduct($_POST['t_code']) == 1) {

                echo json_encode(array('have' => true));

            } else {

                if ($this->users_model->insertProduct($_POST['t_description'], $_POST['t_code']) === true) {

                    echo json_encode(array('successful' => true));

                }
            }
        }
    }

    public function createProfile()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {


            if ($this->users_model->verifyProfileName($_POST['t_profile_name']) == 1) {
                echo json_encode(array('have' => true));
            } else {
                if ($this->users_model->insertProfile($_POST['t_profile_name']) === true) {
                    echo json_encode(array('successful' => true));
                }
            }
        }
    }


}
