<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CrudUsersController extends CI_Controller
{


    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */


    public function index()
    {}

    public function show()
    {

        $arrays = array(
            'content' => 'update_users_view',
            'id' => array(),
            'nome' => array(),
            'login' => array(),
            'email' => array(),
            'senha' => array(),
            'data_criacao' => array(),
            'temp_expired_senha' => array(),
            'cod_autorizacao' => array(),
            'status_usuario' => array(),
            'cod_pessoa' => array(),
            'identified' => array()

        );

        $i = 0;
        foreach ($this->updates_model->getUsersFilter($_GET['identified']) as $key) {

            $arrays['id'][$i] = $key['id'];
            $arrays['nome'][$i] = $key['nome'];
            $arrays['login'][$i] = $key['login'];
            $arrays['email'][$i] = $key['email'];
            $arrays['senha'][$i] = $key['senha'];
            $arrays['data_criacao'][$i] = $key['data_criacao'];
            $arrays['temp_expired_senha'][$i] = $key['temp_expired_senha'];
            $arrays['cod_autorizacao'][$i] = $key['cod_autorizacao'];
            $arrays['status_usuario'][$i] = $key['status_usuario'];
            $arrays['cod_pessoa'][$i] = $key['cod_pessoa'];
            $arrays['identified'][$i] = $key['identified'];
            $i++;
        }

        //$this->updates_model->update("gamedsdsd","kabaite","naelsondsdsd.g.saraiva@gmail.com","asasas","07-04-2019 10:54:03",null,null,null,"978c4546701552816eadf6639f10f3d4");
        $this->load->view('template', $arrays);
    }


    public function delete()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' AND !empty($_POST['sendIdentified'])) {

            if ($this->updates_model->delete($_POST['sendIdentified']) === true) {
                echo json_encode(array("deleted" => true));

            }
        }else {
            echo json_encode(array("exception" => true));
        }


    }


    public function update()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' AND !empty($_POST['t_identified'])) {


            if ($this->updates_model->update($_POST['t_name'],$_POST['t_login'],$_POST['t_email'],$_POST['t_password'],$_POST['t_data_create'],$_POST['t_cod_authorization'],$_POST['t_status_people'],$_POST['t_cod_people'], $_POST['t_identified']) == 1) {

                echo json_encode(array("successful" => true));

            } else {
                echo json_encode(array("exception" => true));
            }

        }


    }


}